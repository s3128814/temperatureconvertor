package nl.utwente.di.FtoC;

public class Converter {

    double convertTemperature(double f){
        return (f - 32) * ((double) 5 / 9);
    }
}
