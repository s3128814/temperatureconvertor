package nl.utwente.di.FtoC;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests the Quoter
 */
public class TestConverter {
    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();
        double temperature = converter.convertTemperature(32);
        Assertions.assertEquals(0, temperature, 0.0, "32 °F");
    }
}
